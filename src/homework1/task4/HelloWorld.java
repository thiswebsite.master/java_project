package homework1.task4;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println(Integer.MIN_VALUE + " < int < " + Integer.MAX_VALUE);
        System.out.println(Long.MIN_VALUE + " < long < " + Long.MAX_VALUE);
        System.out.println(Short.MIN_VALUE + " < short < " + Short.MAX_VALUE);
        System.out.println(Byte.MIN_VALUE + " < byte < " + Byte.MAX_VALUE);
    }
}
