package homework7.task2;

public interface Builder {
    public Builder setFullName(String name, String lastName);

    public Builder setUniversity(String university);

    Student getResult();
}
