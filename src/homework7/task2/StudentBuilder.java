package homework7.task2;

import java.util.Date;

public class StudentBuilder implements Builder {
    private String name;
    private String lastName;
    private Date dateOfBirth;
    private int studyYear;
    private String birthCity;
    private String university;

    @Override
    public Builder setFullName(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
        return this;
    }

    @Override
    public Builder setUniversity(String university) {
        this.university = university;
        return this;
    }

    public Student getResult() {
        System.out.println(new Student(this.name, this.lastName, this.dateOfBirth, this.studyYear, this.birthCity, this.university));
        return null;
    }
}
