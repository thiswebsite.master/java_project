package homework7.task2;

import java.util.Date;

public class Student {
    private String name;
    private String lastName;
    private Date dateOfBirth;
    private int studyYear;
    private String birthCity;
    private String university;


    public Student(String name, String lastName, Date dateOfBirth, int studyYear, String birthCity, String university) {
        this.name = name;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.studyYear = studyYear;
        this.birthCity = birthCity;
        this.university = university;
    }


    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public int getStudyYear() {
        return studyYear;
    }

    public String getBirthCity() {
        return birthCity;
    }

    public String getUniversity() {
        return university;
    }

    @Override
    public String toString() {
        return "Студент с " +
                "именем " + name +
                ", фамилией " + lastName +
                ", днём рождения " + dateOfBirth +
                ", курс " + studyYear +
                ", родился в городе " + birthCity +
                ", университет " + university +
                '.';
    }
}
