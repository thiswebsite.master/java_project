package homework7.task2;

public class Main {
    public static void main(String[] args) {
        Student student1 = new StudentBuilder()
                .setFullName("Игорь", "Петров")
                .getResult();

        Student student2 = new StudentBuilder()
                .setUniversity("БФУ им Канта")
                .getResult();
    }
}
