package homework7.task1;

public final class Earth {
    private static Earth instance;

    private Earth() {
    }

    public static Earth getInstance() {
        if (instance == null) {
            instance = new Earth();
            System.out.println("Только что создался новый объект Земля");
        }
        return instance;
    }

    @Override
    public String toString() {
        return "Earth";
    }
}

