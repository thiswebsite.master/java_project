package homework4.task1;

public class Person {
    private String fullName;
    private int age;

    public Person(String fullName, int age) {
        this.fullName = fullName;
        this.age = age;
    }

    public Person() {
        this("Someone", 30);
    }

    public void move() {
        System.out.println(this.fullName + " " + this.age + " лет идёт");
    }

    public void talk() {
        System.out.println(this.fullName + " " + this.age + " лет говорит");
    }
}
