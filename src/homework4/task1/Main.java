package homework4.task1;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person();
        Person person2 = new Person("John", 35);

        person1.move();
        person2.talk();
    }
}