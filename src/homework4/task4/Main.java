package homework4.task4;

public class Main {
    public static void main(String[] args) {

        Book book1 = new Book("Мастер и Маргарита", "Булгаков", 2001);
        Book book2 = new Book("Война и мир", "Толстой", 2001);
        Book book3 = new Book("Словарь", "Ожегов", 2020);
        Book book4 = new Book("Заводной апельсин", "Берджесс", 1962);
        Book book5 = new Book("Заводной апельсин", "Берджесс", 1962);

        HomeLibrary homeLibrary = new HomeLibrary();

        homeLibrary.addBooks(book1, book2, book3, book4, book5);
        homeLibrary.getHomeLibrary();

        homeLibrary.removeBooks(book5);
        homeLibrary.getHomeLibrary();

        homeLibrary.searchBooksByYear(2020, 2001, 2030);

        homeLibrary.sortByYear();
        homeLibrary.sortByBookName();
        homeLibrary.sortByAuthor();
    }
}
