package homework4.task4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class HomeLibrary {
    private List<Book> homeLibrary;

    public HomeLibrary() {
        this.homeLibrary = new ArrayList<>();
    }

    public void getHomeLibrary() {
        if (isEmptyCheck(homeLibrary)) {
            return;
        }

        StringBuilder booksList = new StringBuilder();
        for (Book book : this.homeLibrary) {
            booksList.append(book.getBookName() + ", ");
        }
        booksList.replace(booksList.lastIndexOf(","), booksList.lastIndexOf(",") + 2, "");
        System.out.println("В библиотеке сейчас находятся книги: " + booksList);
    }

    public void addBooks(Book... books) {
        String booksList = stringBuilder(books);
        if (booksList.equals("нет книг")) {
            System.out.println("Выберите книги для добавления в библиотеку.");
        } else {
            System.out.println("Добавляем в библиотеку книги: " + booksList);
            homeLibrary.addAll(Arrays.asList(books));
        }
    }

    public void removeBooks(Book... books) {
        if (isEmptyCheck(homeLibrary)) {
            return;
        }
        String booksList = stringBuilder(books);
        if (booksList.equals("нет книг")) {
            System.out.println("Выберите книги, чтобы убрать их из библиотеки.");
        } else {
            System.out.println("Убираем из библиотеки книги: " + booksList);
        }

        // если нужно убрать 1 такую книгу:
        for (Book book : books) {
            homeLibrary.remove(book);
        }
/**     Если нужно убрать все такие книги:
 *       homeLibrary.removeAll(Arrays.asList(books));
 */
    }

    public void searchBooksByYear(int... publishYears) {
        if (isEmptyCheck(homeLibrary)) {
            return;
        }
        StringBuilder booksList = new StringBuilder();
        for (int year : publishYears) {
            for (int i = 0; i < homeLibrary.size(); i++) {
                if (year == homeLibrary.get(i).getPublishYear()) {
                    booksList.append(homeLibrary.get(i).getBookName() + ", ");
                }
                if (i == homeLibrary.size() - 1 && !booksList.isEmpty()) {
                    booksList.replace(booksList.lastIndexOf(","), booksList.lastIndexOf(",") + 2, "");
                }
            }
            if (booksList.isEmpty()) {
                System.out.println("На текущий момент в библиотеке нет книг, изданных в " + year + " году");
            } else {
                System.out.println("В " + year + " году издан(ы) " + booksList);
                booksList.replace(0, booksList.length(), "");
            }
        }
    }

    public void sortByYear() {
        if (isEmptyCheck(homeLibrary)) {
            return;
        }
        sortBooks(Comparator.comparing(Book::getPublishYear));
    }

    public void sortByBookName() {
        if (isEmptyCheck(homeLibrary)) {
            return;
        }
        sortBooks(Comparator.comparing(Book::getBookName));
    }

    public void sortByAuthor() {
        if (isEmptyCheck(homeLibrary)) {
            return;
        }
        sortBooks(Comparator.comparing(Book::getAuthorName));
    }

    private static String stringBuilder(Book... books) {
        if (books.length == 0) {
            return "нет книг";
        }
        StringBuilder booksList = new StringBuilder();
        for (Book book : books) {
            booksList.append(book.getBookName() + ", ");
        }
        booksList.replace(booksList.lastIndexOf(","), booksList.lastIndexOf(",") + 2, "");
        return booksList.toString();
    }

    private static boolean isEmptyCheck(List<Book> homeLibrary) {
        if (homeLibrary.isEmpty()) {
            System.out.println("В библиотеке сейчас нет книг.");
            return true;
        } else {
            return false;
        }
    }

    private void sortBooks(Comparator<Book> comparator) {
        List<Book> booksSorted = new ArrayList<>(homeLibrary);
        booksSorted.sort(comparator);
        showHomeLibrarySorted(booksSorted);
    }

    private void showHomeLibrarySorted(List<Book> newBooks) {
        if (isEmptyCheck(newBooks)) {
            return;
        }

        StringBuilder booksList = new StringBuilder();
        for (Book book : newBooks) {
            booksList.append(book.getBookName() + ", ");
        }
        booksList.replace(booksList.lastIndexOf(","), booksList.lastIndexOf(",") + 2, "");
        System.out.println("Книги в отсортированном порядке: " + booksList);
    }
}
