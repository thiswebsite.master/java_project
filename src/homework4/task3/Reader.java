package homework4.task3;

import java.util.List;

public class Reader {
    private String fio;
    private String id;
    private String faculty;
    private String dob;
    private String phone;

    public Reader(String fio, String id, String faculty, String dob, String phone) {
        this.fio = fio;
        this.id = id;
        this.faculty = faculty;
        this.dob = dob;
        this.phone = phone;
    }

    public String getFio() {
        return fio;
    }

    public String getId() {
        return id;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getDob() {
        return dob;
    }

    public String getPhone() {
        return phone;
    }

    public void takeBook(int quantity) {
        System.out.println(this.getFio() + " взял " + quantity + " книг(и)");
    }

    public void takeBook(String... books) {
        StringBuilder booksList = new StringBuilder();
        for (int i = 0; i < books.length; i++) {
            if (i == books.length - 1) {
                booksList.append(books[i]);
            } else {
                booksList.append(books[i] + ", ");
            }
        }
        System.out.println(this.getFio() + " взял книги: " + booksList);
    }

    public void takeBook(Book... books) {
        StringBuilder booksList = new StringBuilder();
        for (int i = 0; i < books.length; i++) {
            if (i == books.length - 1) {
                booksList.append(books[i].getName());
            } else {
                booksList.append(books[i].getName() + ", ");
            }
        }
        System.out.println(this.getFio() + " взял книги: " + booksList);
    }

    public void returnBook(int quantity) {
        System.out.println(this.getFio() + " вернул " + quantity + " книг(и)");
    }

    public void returnBook(String[] books) {
        StringBuilder booksList = new StringBuilder();
        for (int i = 0; i < books.length; i++) {
            if (i == books.length - 1) {
                booksList.append(books[i]);
            } else {
                booksList.append(books[i] + ", ");
            }
        }
        System.out.println(this.getFio() + " вернул книги: " + booksList);
    }

    public void returnBook(List<Book> books) {
        StringBuilder booksList = new StringBuilder();
        for (int i = 0; i < books.size(); i++) {
            if (i == books.size() - 1) {
                booksList.append(books.get(i).getName());
            } else {
                booksList.append(books.get(i).getName() + ", ");
            }
        }
        System.out.println(this.getFio() + " вернул книги: " + booksList);
    }
}
