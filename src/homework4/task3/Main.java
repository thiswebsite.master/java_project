package homework4.task3;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Reader reader1 = new Reader("Петров В. В.", "345", "Биология", "11.02.1999", "89116785567");
        Reader reader2 = new Reader("Иванов И. В.", "777", "Экономика", "15.07.1999", "89116455565");
        Reader reader3 = new Reader("Светлов И. С.", "37", "Литература", "01.03.1999", "89116755763");

        Book book1 = new Book("Мастер и Маргарита", "Булгаков");
        Book book2 = new Book("Война и мир", "Толстой");
        Book book3 = new Book("Словарь", "Ожегов");

        reader1.takeBook(3);
        reader2.takeBook("Приключения", "Словарь", "Энциклопедия");
        reader3.takeBook(book1, book2, book3);

        reader1.returnBook(5);
        reader2.returnBook(new String[]{"Роман", "Приключения"});
        reader3.returnBook(Arrays.asList(book1, book2));
    }
}
