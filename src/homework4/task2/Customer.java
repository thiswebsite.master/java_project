package homework4.task2;

public class Customer {
    private String lastName;
    private String name;
    private String surname;
    private String address;
    private String cardNumber;
    private String accountNumber;

    public Customer(String lastName, String name, String surname, String address, String cardNumber, String accountNumber) {
        this.lastName = lastName;
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSurname() {
        return surname;
    }

    public String getAddress() {
        return address;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }
}

