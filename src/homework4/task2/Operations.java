package homework4.task2;

import java.util.List;

public class Operations {

    public static void getNameByMaxLastName(List<Customer> customers) {
        String winner = "";
        String winnerName = "";
        for (Customer customer : customers) {
            if (customer.getLastName().length() > winner.length()) {
                winner = customer.getLastName();
                winnerName = customer.getName();
            }
        }
        System.out.println(winnerName);
    }

    public static void getAddressByCardNumber(List<Customer> customers) {
        for (Customer customer : customers) {
            if (customer.getCardNumber().startsWith("5")) {
                System.out.println(customer.getAddress());
            }
        }
    }

    public static void getBySurname(List<Customer> customers) {
        for (Customer customer : customers) {
            if (customer.getSurname().equals("Евгеньевич")) {
                System.out.println(customer.getName() + " " + customer.getSurname() + " " + customer.getLastName());
            }
        }
    }
}

