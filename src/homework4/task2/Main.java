package homework4.task2;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Customer customer1 = new Customer("Пресняков", "Игорь", "Валерьевич", "Москва, ул. Великолукская 35, 21", "5764897333776661", "407263554008877641");
        Customer customer2 = new Customer("Иванов", "Олег", "Евгеньевич", "Москва, ул. Смольная 15, 22", "3764897333776662", "407263554008877642");
        Customer customer3 = new Customer("Смоляков", "Михаил", "Игнатьевич", "Москва, ул. Красная 2, 23", "5764897333776663", "407263554008877643");
        Customer customer4 = new Customer("Денисова", "Ксения", "Евгеньевна", "Москва, ул. Октябрьская 4, 24", "2764897333776664", "407263554008877644");
        Customer customer5 = new Customer("Сидоров", "Антон", "Петрович", "Москва, ул. Лесная 3, 25", "5764897333776665", "407263554008877645");

        List<Customer> customers = Arrays.asList(customer1, customer2, customer3, customer4, customer5);

        Operations.getNameByMaxLastName(customers);
        Operations.getAddressByCardNumber(customers);
        Operations.getBySurname(customers);

    }
}
