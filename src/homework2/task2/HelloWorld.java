package homework2.task2;

public class HelloWorld {
    public static void main(String[] args) {
        int n = 8;
        int count = 0;
        for (int num = 1; num < n; num++) {
            if (num % 2 != 0 && num % 3 != 0 && num % 5 != 0) {
                count++;
            }
        }
        System.out.println(count);
    }
}
