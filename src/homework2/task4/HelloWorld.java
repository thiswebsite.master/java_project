package homework2.task4;

public class HelloWorld {
    public static void main(String[] args) {
        int num = 731;
        String str = Integer.toString(num);
        int symbolCount = str.length();
        int symbolMultiply = 1;
        for (int i = 0; i < str.length(); i++) {
            symbolMultiply *= Integer.parseInt(str.substring(i, i + 1));
        }
        int symbolCubeSum = 0;
        for (int i = 0; i < str.length(); i++) {
            int symb = Integer.parseInt(str.substring(i, i + 1));
            symbolCubeSum += symb * symb * symb;
        }
        int firstLastSymbolSum = 0;
        for (int i = 0; i < str.length(); i++) {
            if (i == 0 || i == str.length() - 1) {
                firstLastSymbolSum += Integer.parseInt(str.substring(i, i + 1));
            }
        }
        System.out.println("Количество цифр: " + symbolCount + ", произведение цифр: " + symbolMultiply + ", сумма кубов цифр: " + symbolCubeSum + ", сумма сумма первой и последней цифр: " + firstLastSymbolSum);
    }
}
