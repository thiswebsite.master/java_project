package homework2.task3;

public class HelloWorld {
    public static void main(String[] args) {
        String str = "     привет  ,   в   этой    строке лишние   пробелы   .     ";
        StringBuilder newStr = new StringBuilder();
        int strStart = 0;
        int strEnd = 0;

        /**
         * Находим начало строки
         */
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != ' ') {
                strStart = i;
                break;
            }
        }

        /**
         * Находим конец строки
         */
        for (int i = str.length() - 1; i < str.length(); i--) {
            if (str.charAt(i) != ' ') {
                strEnd = i;
                break;
            }
        }

        /**
         * Составляем новую строку без повторяющихся пробелов
         */
        for (int i = strStart; i <= strEnd; i++) {
            if (str.charAt(i) == ' ' && str.charAt(i + 1) != ' ') {
                newStr.append(" ");
            } else if (str.charAt(i) != ' ') {
                newStr.append(str.charAt(i));
            }
        }

        /**
         * Проверяем, нет ли в строке пробелов перед знаками пунктуации, и удаляем их
         */
        for (int i = 0; i < newStr.length(); i++) {
            if ((newStr.charAt(i) == ',') || (newStr.charAt(i) == '.')) {
                if (newStr.charAt(i - 1) == ' ') {
                    newStr.deleteCharAt(i - 1);
                }
            }
        }
        System.out.println(newStr);
    }
}
