package homework2.task5;

public class HelloWorld {
    public static void main(String[] args) {
        String str = "сегодня мы купили 1 яблоко, 1 грушу и 2 апельсина";
        int countOne = 0;
        int countTwo = 0;
        int countThree = 0;
        String one = Integer.toString(1);
        String two = Integer.toString(2);
        String three = Integer.toString(3);
        for (int i = 0; i < str.length(); i++) {
            if (str.substring(i, i + 1).equals(one)) {
                countOne++;
            }
            if (str.substring(i, i + 1).equals(two)) {
                countTwo++;
            }
            if (str.substring(i, i + 1).equals(three)) {
                countThree++;
            }
        }
        System.out.println("Количество 1: " + countOne);
        System.out.println("Количество 2: " + countTwo);
        System.out.println("Количество 3: " + countThree);
    }
}