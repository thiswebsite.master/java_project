package homework2.task1;

public class HelloWorld {
    public static void main(String[] args) {
        int count = 3;
        if (count % 2 == 0) {
            count = (int) Math.pow(count, 10);
        } else if (count % 2 != 0) {
            count = (int) Math.pow(count, 3);
        } else {
            count = 0;
        }
        System.out.println(count);
    }
}
