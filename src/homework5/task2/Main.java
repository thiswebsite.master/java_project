package homework5.task2;

public class Main {
    public static void main(String[] args) {
        Printable[] printables = new Printable[4];

        printables[0] = new Book("Мастер и Маргарита", "Булгаков");
        printables[1] = new Book("Война и мир", "Толстой");
        printables[2] = new Magazine("Лиза", 33);
        printables[3] = new Magazine("Домашний уют", 143);

        for (Printable item : printables) {
            item.print();
        }

        Magazine.printMagazines(printables);
        Book.printBooks(printables);
    }
}
