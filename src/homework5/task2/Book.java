package homework5.task2;

public class Book implements Printable {
    private String bookName;
    private String authorName;

    public Book(String bookName, String authorName) {
        this.bookName = bookName;
        this.authorName = authorName;
    }

    @Override
    public String toString() {
        return "Книга под названием " + bookName +
                ", автор " + authorName;
    }

    @Override
    public void print() {
        System.out.println(this);
    }

    public static void printBooks(Printable[] printable) {
        for (Printable item : printable) {
            if (item instanceof Book) {
                System.out.println(((Book) item).bookName);
            }
        }
    }
}
