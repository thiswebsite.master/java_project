package homework5.task2;

public class Magazine implements Printable {
    private String magazineName;
    private int number;

    public Magazine(String magazineName, int number) {
        this.magazineName = magazineName;
        this.number = number;
    }

    @Override
    public String toString() {
        return "Журнал под названием " + magazineName +
                ", номер выпуска " + number;
    }

    @Override
    public void print() {
        System.out.println(this);
    }

    public static void printMagazines(Printable[] printable) {
        for (Printable item : printable) {
            if (item instanceof Magazine) {
                System.out.println(((Magazine) item).magazineName);
            }
        }
    }
}
