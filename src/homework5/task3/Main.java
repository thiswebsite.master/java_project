package homework5.task3;

public class Main {
    public static void main(String[] args) {
        User user1 = new User("Петя", "strongpass");

        user1.auth("Петя", "strongpass");

        InternetShop.viewAllCatalogs();
        InternetShop.viewCatalog("CLOTHES");
        InternetShop.addToCart("Glasses");

        InternetShop.addToCart("Flats");
        InternetShop.buy();

    }
}
