package homework5.task3;

import java.util.ArrayList;

public enum Catalogs {
    CLOTHES("Shirt", "Pants", "Skirt"),
    SHOES("Flats", "Boots", "Slippers"),
    ACCESSORIES("Watch", "Tie", "Gloves");

    private String product1;
    private String product2;
    private String product3;

    Catalogs(String product1, String product2, String product3) {
        this.product1 = product1;
        this.product2 = product2;
        this.product3 = product3;
    }

    public static String getProductsByCatalog(Catalogs catalogName) {
        switch (catalogName) {
            case CLOTHES:
                return Catalogs.CLOTHES.product1 + ", " + Catalogs.CLOTHES.product2 + ", " + Catalogs.CLOTHES.product3;
            case SHOES:
                return Catalogs.SHOES.product1 + ", " + Catalogs.SHOES.product2 + ", " + Catalogs.SHOES.product3;
            case ACCESSORIES:
                return Catalogs.ACCESSORIES.product1 + ", " + Catalogs.ACCESSORIES.product2 + ", " + Catalogs.ACCESSORIES.product3;
        }
        return "Such catalogue does not exist!";
    }

    public static ArrayList<String> getAllProducts() {
        ArrayList<String> values = new ArrayList<>();
        for (Catalogs catalog : Catalogs.values()) {
            values.add(catalog.product1);
            values.add(catalog.product2);
            values.add(catalog.product3);
        }
        return values;
    }
}
