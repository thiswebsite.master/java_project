package homework5.task3;

public class User {
    private String login;
    private String pass;

    public User(String login, String pass) {
        this.login = login;
        this.pass = pass;
    }

    public void auth(String login, String pass) {
        if (login.equals(this.login) && pass.equals(this.pass)) {
            System.out.println(this + " аутентифицирован");
        } else {
            System.out.println("Аутентификация не успешна");
        }
    }

    @Override
    public String toString() {
        return "Пользователь с логином " + login;
    }


}
