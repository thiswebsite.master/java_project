package homework5.task3;

import java.util.ArrayList;

public class InternetShop {
    public static void viewAllCatalogs() {
        for (Catalogs catalog : Catalogs.values()) {
            System.out.println(catalog);
        }
    }

    public static void viewCatalog(String wantedCatalogName) {
        ArrayList<String> values = new ArrayList<>();
        for (Catalogs catalog : Catalogs.values()) {
            values.add(catalog.toString());
        }
        if (values.contains(wantedCatalogName)) {
            System.out.println(Catalogs.getProductsByCatalog(Catalogs.valueOf(wantedCatalogName)));
        } else {
            System.out.println("Such catalogue does not exist!");
        }
    }

    public static void addToCart(String wantedProduct) {
        if (Catalogs.getAllProducts().contains(wantedProduct)) {
            Cart.addedProducts.add(wantedProduct);
            System.out.println(wantedProduct + " has been added to the cart!" + '\n' + "Cart now has: " + Cart.getAddedProducts());
        } else {
            System.out.println("Such product does not exist!");
        }
    }

    public static void buy() {
        if (Cart.addedProducts.isEmpty()) {
            System.out.println("The cart is empty");
        } else {
            System.out.println("You are buying " + Cart.getAddedProducts());
            Cart.addedProducts.clear();
            System.out.println("Thank you for your purchase! Cart now is empty: " + Cart.getAddedProducts());

        }
    }
}
