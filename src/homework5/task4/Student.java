package homework5.task4;

public class Student {
    private String firstName;
    private String lastName;
    private int group;
    private double averageMark;

    public Student(String firstName, String lastName, int group, double averageMark) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.group = group;
        this.averageMark = averageMark;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getGroup() {
        return group;
    }

    public double getAverageMark() {
        return averageMark;
    }

    public Integer getScholarship(double averageMark) {
        int scholarship;
        if (averageMark == 5) {
            return scholarship = 100;
        } else {
            return scholarship = 80;
        }
    }

    @Override
    public String toString() {
        return "Студент " + firstName + " " + lastName +
                " из группы " + group +
                " со средней оценкой " + averageMark;
    }
}
