package homework5.task4;

public class Aspirant extends Student {
    private String paper;

    public Aspirant(String firstName, String lastName, int group, double averageMark, String paper) {
        super(firstName, lastName, group, averageMark);
        this.paper = paper;
    }

    @Override
    public Integer getScholarship(double averageMark) {
        return super.getScholarship(averageMark) + 100;
    }

    @Override
    public String toString() {
        return "Аспирант " + super.getFirstName() + " " + super.getLastName() +
                " из группы " + super.getGroup() +
                " со средней оценкой " + super.getAverageMark();
    }


}

