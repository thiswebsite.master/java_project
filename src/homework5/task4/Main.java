package homework5.task4;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student("Иван", "Боярский", 3, 4.5);
        Student student2 = new Student("Кристина", "Каялова", 1, 3);
        Student student3 = new Student("Савелий", "Местников", 2, 5);
        Student student4 = new Aspirant("Валерий", "Константинов", 5, 4.6, "Труды Лермонтова");
        Student student5 = new Aspirant("Светлана", "Краеведова", 5, 5, "Война в произведениях русских писателей 19 века");

        List<Student> students = Arrays.asList(student1, student2, student3, student4, student5);

        for (Student student : students) {
            System.out.println(student + " получает стипендию " + student.getScholarship(student.getAverageMark()) + " грн");
        }

    }
}
