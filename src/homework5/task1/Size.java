package homework5.task1;

public enum Size {
    noSize(0),
    XXS(32),
    XS(34),
    S(36),
    M(38),
    L(40);

    private final int euroSize;

    Size(int euroSize) {
        this.euroSize = euroSize;
    }

    public String getDescription() {
        return this.euroSize == 32 ? "Детский размер" : "Взрослый размер";
    }
}
