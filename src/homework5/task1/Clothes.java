package homework5.task1;

public abstract class Clothes {
    private Size size;
    private double price;
    private String color;

    public Clothes(Size size, double price, String color) {
        this.size = size;
        this.price = price;
        this.color = color;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() +
                "  размер: " + size +
                ", цена: " + price +
                ", цвет: " + color;
    }
}
