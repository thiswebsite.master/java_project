package homework5.task1;

public class Tshirt extends Clothes implements menClothes, womenClothes {

    public Tshirt(Size size, double price, String color) {
        super(size, price, color);
    }

    public void dressMan() {
        System.out.println(super.toString());
    }

    public void dressWoman() {
        System.out.println(super.toString());
    }
}
