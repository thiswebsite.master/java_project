package homework5.task1;

public class Pants extends Clothes implements menClothes, womenClothes {

    public Pants(Size size, double price, String color) {
        super(size, price, color);
    }

    public void dressMan() {
        System.out.println(super.toString());
    }

    public void dressWoman() {
        System.out.println(super.toString());
    }
}
