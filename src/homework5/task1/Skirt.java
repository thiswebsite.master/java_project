package homework5.task1;

public class Skirt extends Clothes implements womenClothes {

    public Skirt(Size size, double price, String color) {
        super(size, price, color);
    }

    public void dressWoman() {
        System.out.println(super.toString());
    }
}
