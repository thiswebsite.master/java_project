package homework5.task1;

import java.util.ArrayList;

public class Atelier {
    public void dressMan(ArrayList<Clothes> clothes) {
        for (Clothes clothes1 : clothes) {
            if (clothes1 instanceof menClothes) {
                System.out.println(clothes1);
            }
        }
    }

    public void dressWoman(ArrayList<Clothes> clothes) {
        for (Clothes clothes1 : clothes) {
            if (clothes1 instanceof womenClothes) {
                System.out.println(clothes1);
            }
        }
    }
}
