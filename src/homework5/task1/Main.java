package homework5.task1;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        ArrayList<Clothes> clothes = new ArrayList<>();

        clothes.add(new Tshirt(Size.XXS, 100, "розовый"));
        clothes.add(new Tshirt(Size.M, 110, "чёрный"));
        clothes.add(new Pants(Size.XS, 200, "белый"));
        clothes.add(new Pants(Size.L, 220, "серый"));
        clothes.add(new Skirt(Size.M, 170, "красный"));
        clothes.add(new Tie(Size.noSize, 120, "зелёный"));

        Atelier atelier = new Atelier();

        atelier.dressWoman(clothes);
        atelier.dressMan(clothes);
    }
}
