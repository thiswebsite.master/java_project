package homework3.task2;

import java.util.Arrays;
import java.util.List;

public class HelloWorld {
    public static void main(String[] args) {
        List<String> queue = Arrays.asList("Миша", "Петя", "Катя", "Игорь");
        int window = 2;
        int time = 15;
        int timeOfVisit = 10;
        int visitor = 0;
        String customers = customers(queue, window, time, timeOfVisit, visitor);
        System.out.println("На " + time + " минуте " + customers);
    }

    public static String customers(List<String> queue, int window, int time, int timeOfVisit, int visitor) {
        /**
         * Находим, сколько посетителей при данном количестве окон за указанное время time 
         * будут полностью обслужены
         */
        for (int i = 0; i < (time - 1) / timeOfVisit; i++) {
            for (int j = 0; j < window && j < queue.size() && visitor < queue.size(); j++) {
                visitor++;
            }
        }
        int inProcess = 0;
        int inQueue = 0;
        StringBuilder inProcessAnswer = new StringBuilder();
        StringBuilder inQueueAnswer = new StringBuilder();

        /**
         * Находим, какие посетители на указанной минуте time стоят у окон, 
         * а какие ждут в очереди
         */
        while (visitor < queue.size()) {
            if (inProcess >= window) {
                inQueueAnswer.append(queue.get(visitor) + ", ");
                inQueue++;
                visitor++;
            } else {
                inProcessAnswer.append(queue.get(visitor) + ", ");
                inProcess++;
                visitor++;
            }
        }
        if (inProcess == 0) {
            return "все посетители будут обслужены!";
        }
        if (inQueue == 0) {
            inProcessAnswer.replace(inProcessAnswer.lastIndexOf(","), inProcessAnswer.lastIndexOf(",") + 2, "");
            return "у окон будут стоять " + inProcessAnswer + ", а в очереди никого не будет.";
        }
        inProcessAnswer.replace(inProcessAnswer.lastIndexOf(","), inProcessAnswer.lastIndexOf(",") + 2, "");
        inQueueAnswer.replace(inQueueAnswer.lastIndexOf(","), inQueueAnswer.lastIndexOf(",") + 2, "");
        return "у окон будут стоять " + inProcessAnswer + ", а в очереди ещё будут " + inQueueAnswer;
    }
}
