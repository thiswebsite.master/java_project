package homework3.task3;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HelloWorld {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(3, 3, 5, 5, 3, 9, 3, 3, 9, 3);
        String answer = repeats(numbers);
        System.out.println(answer);
    }

    public static String repeats(List<Integer> numbers) {
        HashMap<Integer, Integer> result = new HashMap<>();
        int repeatsNum = 0;
        for (int i = 0; i < numbers.size(); i++) {
            if (result.containsKey(numbers.get(i))) {
                continue;
            } else {
                for (int j = 0; j < numbers.size(); j++) {
                    if (numbers.get(i) == numbers.get(j)) {
                        repeatsNum++;
                    }
                }
                if (repeatsNum > 1) {
                    result.put((Integer) numbers.get(i), repeatsNum);
                }
                repeatsNum = 0;
            }
        }
        StringBuilder answer = new StringBuilder();
        for (Map.Entry<Integer, Integer> entry : result.entrySet()) {
            answer.append("Цифра " + entry.getKey() + " повторяется " + entry.getValue() + " раз(а) \n");
        }
        return answer.toString();
    }
}
