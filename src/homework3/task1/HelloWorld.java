package homework3.task1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HelloWorld {
    public static void main(String[] args) {
        double median = medianOfTwoArrays();
        System.out.println(median);
    }

    public static double medianOfTwoArrays() {
        List<Integer> list1 = Arrays.asList(1, 2);
        List<Integer> list2 = Arrays.asList(3, 4);
        List<Integer> result = new ArrayList<>();
        int pointer1 = 0;
        int pointer2 = 0;
        double median = 0;
        while (pointer1 < list1.size() && pointer2 < list2.size()) {
            if (list1.get(pointer1) < list2.get(pointer2)) {
                result.add(list1.get(pointer1));
                pointer1++;
            } else {
                result.add(list2.get(pointer2));
                pointer2++;
            }
        }
        if (pointer1 < list1.size()) {
            while (pointer1 < list1.size()) {
                result.add(list1.get(pointer1));
                pointer1++;
            }
        }
        if (pointer2 < list2.size()) {
            while (pointer2 < list2.size()) {
                result.add(list2.get(pointer2));
                pointer2++;
            }
        }
        if (result.size() % 2 == 0) {
            double element1 = result.get(Integer.valueOf(result.size() / 2));
            double element2 = result.get(Integer.valueOf((result.size() / 2) - 1));
            median = Double.valueOf((element1 + element2) / 2);
        } else {
            median = result.get(Integer.valueOf(result.size() / 2));
        }
        return median;
    }
}
