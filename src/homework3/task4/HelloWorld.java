package homework3.task4;

import java.util.*;

public class HelloWorld {
    public static void main(String[] args) {
        HashMap<String, Integer> monthStart = new LinkedHashMap<>();
        monthStart.put("Картошка", 30);
        monthStart.put("Перец", 20);
        monthStart.put("Кукуруза", 80);

        HashMap day1 = new LinkedHashMap<>();
        day1.put("Картошка", 0);
        day1.put("Перец", 1);
        day1.put("Кукуруза", 0);

        HashMap day2 = new LinkedHashMap<>();
        day2.put("Картошка", 2);
        day2.put("Перец", 1);
        day2.put("Кукуруза", 0);

        HashMap dayN = new LinkedHashMap<>();
        dayN.put("Картошка", 4);
        dayN.put("Перец", 0);
        dayN.put("Кукуруза", 0);

        List<HashMap<String, Integer>> dailyUse = Arrays.asList(day1, day2, dayN);

        String answer = isLeft(monthStart, dailyUse);
        System.out.println(answer);
    }

    public static String isLeft(HashMap<String, Integer> monthStart, List<HashMap<String, Integer>> dailyUse) {

        for (Map.Entry<String, Integer> entry : monthStart.entrySet()) {
            for (int i = 0; i < dailyUse.size(); i++) {
                for (Map.Entry element : dailyUse.get(i).entrySet()) {
                    if (entry.getKey() == element.getKey()) {
                        entry.setValue(entry.getValue() - (int) element.getValue());
                    }
                }
            }
        }
        StringBuilder answer = new StringBuilder();
        for (Map.Entry entry : monthStart.entrySet()) {
            answer.append("\"" + entry.getKey().toString() + "\": " + entry.getValue().toString() + ", ");
        }
        answer.replace(answer.lastIndexOf(","), answer.lastIndexOf(",") + 2, "");
        return "Остаток {" + answer + "}";
    }
}
