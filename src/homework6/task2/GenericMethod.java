package homework6.task2;

import java.util.List;

public class GenericMethod {
    public static <Type> List<Type> changePositions(List<Type> list, int indexA, int indexB) {
        if (indexA < list.size() && indexB < list.size()) {
            Type valueA = list.get(indexA);
            list.set(indexA, list.get(indexB));
            list.set(indexB, valueA);
            return list;
        } else {
            System.out.println("Such element(s) don't exist in a list. Choose different index(es)");
            return null;
        }
    }
}
