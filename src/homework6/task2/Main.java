package homework6.task2;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> listNum = new ArrayList<>();
        listNum.add(3);
        listNum.add(7);
        listNum.add(9);
        listNum.add(33);

        List<Long> listLong = new ArrayList<>();
        listLong.add(1L);
        listLong.add(2L);
        listLong.add(3L);
        listLong.add(7L);

        List<Float> listFloat = new ArrayList<>();
        listFloat.add(1.5F);
        listFloat.add(9.1F);
        listFloat.add(29F);
        listFloat.add(33F);

        List<Double> listDouble = new ArrayList<>();
        listDouble.add(1.555);
        listDouble.add(9.11111);
        listDouble.add(29.0);
        listDouble.add(33.44);

        List<String> listString = new ArrayList<>();
        listString.add("cat");
        listString.add("dog");
        listString.add("eel");
        listString.add("fox");

        System.out.println(GenericMethod.changePositions(listNum, 0, 1));
        System.out.println(GenericMethod.changePositions(listLong, 1, 2));
        System.out.println(GenericMethod.changePositions(listFloat, 1, 2));
        System.out.println(GenericMethod.changePositions(listDouble, 1, 2));
        System.out.println(GenericMethod.changePositions(listString, 1, 7));


    }
}
