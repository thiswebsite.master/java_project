package homework6.task1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GenericMethod {
    public static <Type> List<Type> filteredByOdd(List<? extends Number> group) {
        List<Type> filtered = new ArrayList<>();
        group.forEach(element -> {
            if (element.doubleValue() % 2 != 0 && element.doubleValue() - Math.floor(element.doubleValue()) == 0) {
                filtered.add((Type) element);
            }
        });
        return filtered;
    }

    public static <Type> List<Type> filteredByNatural(List<? extends Number> group) {
        List<Type> filtered = new ArrayList<>();
        filtered.addAll((Collection<? extends Type>) group);
        group.forEach(element -> {
            if (element.doubleValue() <= 1) {
                filtered.remove((Type) element);
            }
            if (element.doubleValue() - Math.floor(element.doubleValue()) > 0) {
                filtered.remove((Type) element);
            }
            for (int i = 2; i < element.doubleValue(); i++) {
                if (element.doubleValue() % i == 0) {
                    filtered.remove((Type) element);
                }
            }
        });
        return filtered;
    }

}
