package homework6.task1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Integer> listNum = new ArrayList<>();
        listNum.add(1);
        listNum.add(2);
        listNum.add(3);
        listNum.add(7);
        listNum.add(9);
        listNum.add(29);
        listNum.add(33);

        List<Long> listLong = new ArrayList<>();
        listLong.add(1L);
        listLong.add(2L);
        listLong.add(3L);
        listLong.add(7L);
        listLong.add(9L);
        listLong.add(29L);
        listLong.add(33L);

        List<Float> listFloat = new ArrayList<>();
        listFloat.add(1.5F);
        listFloat.add(2F);
        listFloat.add(3F);
        listFloat.add(7F);
        listFloat.add(9.1F);
        listFloat.add(29F);
        listFloat.add(33F);

        List<Double> listDouble = new ArrayList<>();
        listDouble.add(1.555);
        listDouble.add(2.0);
        listDouble.add(3.0);
        listDouble.add(7.0);
        listDouble.add(9.11111);
        listDouble.add(29.0);
        listDouble.add(33.0);

        System.out.println(GenericMethod.filteredByOdd(listNum));
        System.out.println(GenericMethod.filteredByOdd(listLong));
        System.out.println(GenericMethod.filteredByOdd(listFloat));
        System.out.println(GenericMethod.filteredByOdd(listDouble));

        System.out.println(GenericMethod.filteredByNatural(listNum));
        System.out.println(GenericMethod.filteredByNatural(listLong));
        System.out.println(GenericMethod.filteredByNatural(listFloat));
        System.out.println(GenericMethod.filteredByNatural(listDouble));
    }
}
