package homework6.task3;

public class Division {
    public static void main(String[] args) {
        System.out.println(division(3, 0));

    }

    public static double division(int a, int b) {
        double res = 0;
        try {
            res = a / b;
        } catch (ArithmeticException ex) {
            System.out.println("Произошло деление на ноль, выберите другой делитель.");
        }
        return res;
    }

}
